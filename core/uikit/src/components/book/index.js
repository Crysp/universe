import chalk from 'chalk';


export default class Book {
    static draft = `
    _______
   /      /,
  /      //
 /______//
(______(/
    `;
    constructor({ color }) {
        this.color = color;
    }
    render() {
        console.log(chalk[this.color](Book.draft));
    }
}