import chalk from 'chalk';


export default class Cactus {
    static draft = `
    ,*-.
    |  |
,.  |  |
| |_|  | ,.
\`---.  |_| |
    |  .--\`
    |  |
    |  |
    `;
    constructor({ color }) {
        this.color = color;
    }
    render() {
        console.log(chalk[this.color](Cactus.draft));
    }
}