import chalk from 'chalk';


export default class Duck {
    static draft = `
       ,~~.
      (  6 )-_,
 (\\___ )=='-'
  \\ .   ) )
   \\ \`-' /
~'\`~'\`~'\`~'\`~
    `;
    constructor({ color }) {
        this.color = color;
    }
    render() {
        console.log(chalk[this.color](Duck.draft));
    }
}