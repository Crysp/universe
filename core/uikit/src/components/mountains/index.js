import chalk from 'chalk';


export default class Mountains {
    static draft = `
        _    .  ,   .           .
    *  / \\_ *  / \\_      _  *        *   /\\'__        *
      /    \\  /    \\,   ((        .    _/  /  \\  *'.
 .   /\\/\\  /\\/ :' __ \\_  \`          _^/  ^/    \`--.
    /    \\/  \\  _/  \\-'\\      *    /.' ^_   \\_   .'\\  *
  /\\  .-   \`. \\/     \\ /==~=-=~=-=-;.  _/ \\ -. \`_/   \\
 /  \`-.__ ^   / .-'.--\\ =-=~_=-=~=^/  _ \`--./ .-'  \`-
/        \`.  / /       \`.~-^=-=~=^=.-'      '-._ \`._
    `;
    constructor({ color }) {
        this.color = color;
    }
    render() {
        console.log(chalk[this.color](Mountains.draft));
    }
}