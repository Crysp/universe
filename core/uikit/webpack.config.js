const path = require('path');


module.exports = {
    entry: {
        index: './src/index.js',
        duck: './src/components/duck/index.js',
        madcat: './src/components/madcat/index.js',
        book: './src/components/book/index.js',
        // mountains: './src/components/mountains/index.js',
        // cactus: './src/components/cactus/index.js'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js',
        libraryTarget: 'umd'
    },
    target: 'node',
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /node_modules/,
            loader: ['babel-loader']
        }]
    }
};