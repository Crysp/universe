#!/usr/bin/env node

'use strict';

const program = require('commander');
const runtimeConfig = require('../lib/runtimeConfig');
const ProjectFactory = require('../lib/factory');
const ProjectTypes = require('../lib/factory').ProjectTypes;


program.parse(process.argv);

const apps = program.args;

if (!apps.length) {
    console.error('"name" is required');
    process.exit(1);
}

apps.forEach(appName => {
    const project = new ProjectFactory({
        type: ProjectTypes.Application,
        namespace: runtimeConfig.namespace,
        name: appName,
        cwd: runtimeConfig.location.app
    });
    project
        .init()
        .then(() => project.install());
    project.extract();
});