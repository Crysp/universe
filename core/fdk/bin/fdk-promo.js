#!/usr/bin/env node

'use strict';

const program = require('commander');
const runtimeConfig = require('../lib/runtimeConfig');
const ProjectFactory = require('../lib/factory');
const ProjectTypes = require('../lib/factory').ProjectTypes;


program.parse(process.argv);

const promos = program.args;

if (!promos.length) {
    console.error('"name" is required');
    process.exit(1);
}

promos.forEach(promoName => {
    const project = new ProjectFactory({
        type: ProjectTypes.Promo,
        namespace: runtimeConfig.namespace,
        name: promoName,
        cwd: runtimeConfig.location.promo
    });
    project
        .init()
        .then(() => project.install());
    project.extract();
});