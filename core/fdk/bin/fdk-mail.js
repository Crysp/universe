#!/usr/bin/env node

'use strict';

const program = require('commander');
const runtimeConfig = require('../lib/runtimeConfig');
const ProjectFactory = require('../lib/factory');
const ProjectTypes = require('../lib/factory').ProjectTypes;


program.parse(process.argv);

const mails = program.args;

if (!mails.length) {
    console.error('"name" is required');
    process.exit(1);
}

mails.forEach(mailName => {
    const project = new ProjectFactory({
        type: ProjectTypes.Email,
        namespace: runtimeConfig.namespace,
        name: mailName,
        cwd: runtimeConfig.location.mail
    });
    project
        .init()
        .then(() => project.install());
    project.extract();
});