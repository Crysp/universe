const gulp = require('gulp');
const through = require('through2');
const browserify = require('browserify');
const babelify = require('babelify');
const source = require('vinyl-source-stream');
const buff = require('vinyl-buffer');
const browserSync = require('browser-sync').create();
const reload = browserSync.reload;


// process.env.NODE_ENV = 'production';


gulp.task('markup', () => (
    browserify({
        entries: ['./src/index.js'],
        standalone: 'render'
    })
        .external(['react', 'react-dom/server', 'mjml'])
        .transform(babelify)
        .bundle()
        .pipe(source('index.html'))
        .pipe(buff())
        .pipe(render())
        .pipe(gulp.dest('dist'))
));


gulp.task('watch', ['default'], () => {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });
    // gulp.watch('./src/images/*', ['images']).on('change', reload);
    gulp.watch('./src/**/*.js', ['markup']).on('change', reload);
});


gulp.task('default', ['markup']);


function render() {
    const Module = module.constructor;
    return through.obj(function (file, enc, cb) {
        const contents = file.contents.toString();
        const renderModule = new Module();
        renderModule.id = file.path;
        renderModule.filename = file.path;
        renderModule.paths = module.paths;
        renderModule._compile(contents, file.path);
        const render = renderModule.exports.default || renderModule.exports;
        file.contents = new Buffer(render());
        this.push(file);
        cb();
    });
}