const gulp = require('gulp');
const babel = require('gulp-babel');
const sourcemaps = require('gulp-sourcemaps');
const concat = require('gulp-concat');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const csso = require('gulp-csso');
const imagemin = require('gulp-imagemin');
const twig = require('gulp-twig');
const browserSync = require('browser-sync').create();
const reload = browserSync.reload;


gulp.task('scripts', () => (
    gulp.src('./src/js/index.js')
        .pipe(sourcemaps.init())
        .pipe(babel())
        .pipe(concat('script.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist/js'))
));
gulp.task('styles', () => (
    gulp.src('./src/scss/index.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(csso())
        .pipe(concat('style.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist/css'))
));
gulp.task('images', () => (
    gulp.src('./src/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/images'))
));
gulp.task('markup', () => (
    gulp.src('./src/views/index.twig')
        .pipe(twig({
            data: require('./src/views/data')
        }))
        .pipe(gulp.dest('dist'))
));
gulp.task('watch', ['default'], () => {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });
    gulp.watch('./src/js/**/*.js', ['scripts']).on('change', reload);
    gulp.watch('./src/scss/**/*.scss', ['styles']).on('change', reload);
    gulp.watch('./src/images/*', ['images']).on('change', reload);
    gulp.watch('./src/views/**/*.twig', ['markup']).on('change', reload);
});
gulp.task('default', ['scripts', 'styles', 'images', 'markup']);