'use strict';

const fs = require('fs');
const path = require('path');
const mergeDeep = require('./mergeObject');

const name = '.fdkrc';
const defaultConfig = {
    namespace: '',
    location: {
        app: './app',
        promo: './promo',
        mail: './mail'
    }
};


function searchForConfig(dir = process.cwd()) {
    const configPath = path.resolve(dir, name);
    if (fs.existsSync(configPath)) {
        return JSON.parse(fs.readFileSync(configPath, 'utf8'));
    } else if (dir !== '/' && fs.existsSync(path.resolve(dir, '..'))) {
        return searchForConfig(path.resolve(dir, '..'));
    }
    return {};
}

const userConfig = searchForConfig();
const config = mergeDeep(defaultConfig, userConfig);

Object.keys(config.location).forEach(name => {
    config.location[name] = path.resolve(process.cwd(), config.location[name]);
});


module.exports = config;