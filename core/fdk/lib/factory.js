'use strict';

const chalk = require('chalk');
const path = require('path');
const fse = require('fs-extra');
const fs = require('fs');
const spawn = require('child_process').spawn;
const mergeObjects = require('./mergeObject');

const ProjectTypes = {
    Application: Symbol('Application'),
    Email: Symbol('Email'),
    Promo: Symbol('Promo')
};
const ignoredPackageFiles = [
    'src',
    'webpack.config.js',
    '.yarnrc',
    '.babelrc',
    '.browserslistrc'
];

function getSourcePath(type) {
    switch (type) {
        case ProjectTypes.Application:
            return path.resolve(__dirname, '../source/app');
        case ProjectTypes.Email:
            return path.resolve(__dirname, '../source/mail');
        case ProjectTypes.Promo:
            return path.resolve(__dirname, '../source/promo');
        default:
            throw new Error(`Unresolved project type "${type}"`);
    }
}

class ProjectFactory {
    constructor({ type, namespace = '', name, cwd }) {
        this.type = type;
        this.rootDir = path.resolve(cwd, name);
        this.relativeRootDir = './' + path.relative(process.cwd(), this.rootDir);
        this.package = {
            name: namespace.length > 0 ? `${namespace}/${name}` : name,
            version: '0.0.0'
        };
        if (!this._isSafeToCreateProject()) {
            console.log(chalk.red(`Can't create new project in ${this.relativeRootDir}. Folder isn't empty.`));
            process.exit(1);
        }
        console.log(this.rootDir);
        console.log(
            chalk.green('Creating new project') +
            ' ' +
            chalk.yellow(this.package.name) +
            chalk.green(' in ') + this.relativeRootDir
        );
    }
    init() {
        const templatePackageInfo = fse.readJsonSync(path.resolve(getSourcePath(this.type), 'package.json'));
        this.package = mergeObjects(templatePackageInfo, this.package);
        fse.ensureDirSync(this.rootDir);
        return fse.writeJson(`${this.rootDir}/package.json`, this.package, { spaces: 4 });
    }
    install() {
        process.chdir(this.rootDir);
        console.log('Installing packages. This might take a couple of minutes.');

        return new Promise((resolve, reject) => {
            const command = 'yarn';
            const args = [
                'install',
                '--save',
                '--save-exact',
                '--loglevel',
                'error',
            ];
            const child = spawn(command, args, { stdio: 'inherit' });
            child.on('close', code => {
                if (code !== 0) {
                    reject({
                        command: `${command} ${args.join(' ')}`,
                    });
                    return;
                }
                resolve();
            });
        });
    }
    extract() {
        return fse.copy(getSourcePath(this.type), this.rootDir, {
            filter: src => !/package\.json$/.test(src)
        }).then(() => fse.outputFile(`${this.rootDir}/.npmignore`, ignoredPackageFiles.join('\n')));
    }
    _isSafeToCreateProject() {
        if (!fs.existsSync(this.rootDir)) return true;
        const dir = fs.readdirSync(this.rootDir);
        return dir.length === 0;
    }
}


module.exports = ProjectFactory;
module.exports.ProjectTypes = ProjectTypes;