import { Duck } from '@universe/uikit';
import { say as cowsay } from 'cowsay';

console.log(cowsay({
    text : "I'm a moooodule",
    e : "oO",
    T : "U "
}));

const yellowDuck = new Duck({ color: 'yellow' });
// const greenBook = new Book({ color: 'green' });
// const mountains = new Mountains({ color: 'blue' });


yellowDuck.render();
// greenBook.render();
// mountains.render();