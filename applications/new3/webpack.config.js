const path = require('path');


module.exports = {
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'index.js',
        libraryTarget: 'umd',
        libraryExport: 'default'
    },
    target: 'node',
    resolve: {
        alias: {
            // важно чтобы пакеты брались только из зависимостей приложения
            // а не из глобального скоупа
            '@universe': path.resolve(__dirname, 'node_modules/@universe')
        }
    },
    module: {
        rules: [{
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: ['babel-loader']
        }]
    }
};