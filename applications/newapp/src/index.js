export default {
    duck: () => {
        console.log(`
   __
 <(o )___
  ( ._> /
   \`---'   hjw
        `);
    },
    lion: () => {
        console.log(`
   _   _
 _/ \\|/ \\_
/\\\\/   \\//\\
\\|/<\\ />\\|/
/\\   _   /\\
\\|/\\ Y /\\|/
 \\/|v-v|\\/
  \\/\\_/\\/
        `);
    },
    pig: () => {
        console.log(`
 ^..^
( oo )  )~
  ,,  ,,
        `);
    }
}