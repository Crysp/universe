# Universe

[![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff.svg)](https://lernajs.io/)

* [Версионирование](#1)
* [Корпоративный NPM](#2)
* [Пространство имен](#3)
* [Frontend Developer Kit](#4)
* [Публикация пакетов](#5)

## Версионирование

[Семантическое Версионирование](https://semver.org/lang/ru/)

![Semantic Versioning](https://nhsconnect.github.io/gpconnect/images/design/semantic-versioning.png)

## Корпоративный NPM

Чтобы наш "говнокод" не стал достоянием общественности, будем использовать собственное хранилище для пакетов.

[Корпоративный NPM](http://192.168.1.19:4873)

Зарегистрироваться в npm

	npm adduser --registry http://192.168.1.19:4873
	
## Пространство имен

Для пакетов можно задавать пространство имен (namespace) чтобы обозначить их отношение к определенной группе. Задается как `@namespace/package`

Внутри проекта введен namespace - `@universe`

Поэтому можно заранее указать в настройках `yarn` что все пакеты с этим namespace будут регистрироваться в нашем NPM

	yarn config set "@universe:registry" "http://192.168.1.19:4873" --global

## Frontend Developer Kit

Frontend Developer Kit - набор типичного фронтедера для реактивной разработки типовых проектов (письма, лендинги и приложения).

Устанавливаем глобально

	yarn global add @universe/fdk@latest

Новое письмо

	fdk mail <mail_name>

Новый лендинг

	fdk promo <promo_name>

Новое приложение

	fdk app <app_name>
	
## Публикация пакетов

* Новый пакет создается с версией 0.0.0
* Пишется функционал
* Файлы пакета добавляются в коммит (пушить не обязательно)
* `lerna publish`
* Выставляется релизная версия пакета - 0.1.0
* ... _two thousand years later_ ...
* Что-то поменялось в пакете
* Изменения добавляются в коммит
* `lerna publish`
* Выставляется необходимая релизная версия пакета